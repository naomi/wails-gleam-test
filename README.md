# wails-gleam-test

This is a [Wails](https://wails.io/) app that uses [Gleam](https://gleam.run/) and [Lustre](https://gleam.run/) on the front-end.

Many thanks to [Wesley Moore's Gleam Tauri Experiment](https://www.wezm.net/v2/posts/2024/gleam-tauri/) and [Erika Rowland's Gleam with Vite blog post](https://erikarow.land/notes/gleam-vite).

![alt text](screenshot.png)

## Building
### Prerequisites 
- Go
- Gleam
- Bun
- Wails
### Build
Build/run the project with
```
wails dev
```