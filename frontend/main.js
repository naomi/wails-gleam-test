import './style.css'

import { main } from './src/wails_gleam_test.gleam'

document.addEventListener("DOMContentLoaded", () => {
    const dispatch = main({});
})
