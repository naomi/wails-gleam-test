import { EventsOn } from "../wailsjs/runtime/runtime";
import { Greet } from "../wailsjs/go/main/App";
import { Ok, Error } from "./gleam.mjs";

export async function greet(string) {
  try {
    return new Ok(await Greet(string))
  } catch (error) {
    return new Error(error.to_string())
  }
}

export function listenForTick(handler) {
  return EventsOn('app:tick', (data) => {
    handler(data)
  })
}

export function from_unix(timestamp) {
  return new Date(timestamp * 1000)
}

export function date_to_string(date) {
  return date;
}