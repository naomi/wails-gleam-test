import { build, defineConfig } from "vite";
import gleam from "vite-gleam";

export default defineConfig({
  clearScreen: false,
  build: {
    rollupOptions: {
      input: 'main.js',
      output: {
        dir: './dist',
        entryFileNames: 'assets/gleam_wails.js'
      }
    }
  },
  plugins: [gleam()]
})